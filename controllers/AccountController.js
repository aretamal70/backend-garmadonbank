const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');

const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechuarr9ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

/**
 * Description
 * @method getAccountsByUserId
 * @param {} req
 * @param {} res
 * @return devuelve las cuentas del usuario logado
 */
function getAccountsByUid(req, res) {
  console.log('GET /apitechuarr9ed/accounts/:uid');
  console.log("user introducido " + req.params.uid);
  var uid = req.params.uid;

  var response = {};
  response.msg = "";

  var query = 'q={"uid":' + uid + '}';
  console.log(query);

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("cliente MLAB creado");

  httpClient.get("accounts?" + query + "&" + mLabAPIKey,
    function (err, resMlab, body) {
      if (err) {
        response = { "msg": "Error obteniendo cuentas de usuario" };
        res.status(500);
      } else {
        response = body;
      }

      console.log(response);
      res.send(response);
    }
  );
}

/**
 * Description
 * @method getAccountsByUserId
 * @param {} req
 * @param {} res
 * @return devuelve las cuentas del usuario logado
 */
function getAccountsByNif(req, res) {
  console.log('GET /apitechuarr9ed/accounts/:uid');
  console.log("user introducido " + req.params.uid);
  var uid = req.params.uid;

  var response = {};
  response.msg = "";

  var query = 'q={"nif":' + uid + '}';
  console.log(query);

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("cliente MLAB creado");

  httpClient.get("accounts?" + query + "&" + mLabAPIKey,
    function (err, resMlab, body) {
      if (err) {
        response = { "msg": "Error obteniendo cuentas de usuario" };
        res.status(500);
      } else {
        response = body;
      }

      console.log(response);
      res.send(response);
    }
  );
}

/**
 * Description
 * @method getAccounts
 * @param {} req
 * @param {} res
 * @return devuelve todas las cuentas
 */
function getAccounts(req, res) {
  console.log('GET /apitechuarr9ed/accounts');

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("cliente MLAB creado");

  httpClient.get("accounts?" + mLabAPIKey,
    function (err, resMlab, body) {
      var response = !err ? body : {
        "msg": "Error obteniendo cuentas del usuario logado"
      }

      res.send(response);
    }
  );
}

//rellena con 0 por delante una cadena
function formatted_string(pad, user_str, pad_pos) {
  if (typeof user_str === 'undefined')
    return pad;
  if (pad_pos == 'l') {
    return (pad + user_str).slice(-pad.length);
  }
  else {
    return (user_str + pad).substring(0, pad.length);
  }
}

/**
 * Description
 * @method getMaxAccount
 * @param {} req
 * @param {} res
 * @return devuelve max id de las cuentas
 */
function getMaxAccount(req, res) {
  console.log('GET /apitechuarr9ed/accounts2');

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("cliente creado");

  httpClient.get("accounts?s={acid: -1}&l=1&" + mLabAPIKey,
    function (err, resMlab, body) {
      var response = !err ? body : {
        "msg": "Error obteniendo cuentas"
      }

      var nextId = 1;
      console.log(body[0].acid);
      if (body[0].id != undefined) {
        nextId = body[0].acid + 1;
      }
      var ccc = formatted_string("0000000000", nextId, "l");
      var iban = "0182041743" + ccc;
      console.log("nextId CCC: " + nextId)
      res.send(response);
    }
  );
}

/**
 * Description
 * @method createAccount
 * @param {} req
 * @param {} res
 * @return crea una cuenta con saldo inicial 0 asociada al usuario logado
 */
function createAccount(req, res) {
  console.log('POST /apitechuarr9ed/accounts');
  console.log("createAccount: " + req.body.uid);

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("createAccount cliente creado");

  httpClient.get("accounts?s={acid: -1}&l=1&" + mLabAPIKey,
    function (err, resMlab, body) {
      var response = !err ? body :
        {
          "msg": "Error obteniendo cuentas"
        }

      var nextId = 1; //Si no hay se empieza por id 1
      if (body[0] != undefined) {
        nextId = body[0].acid + 1;
      }
      console.log("nextId ccc: " + nextId)

      var ccc = formatted_string("0000000000", nextId, "l");
      var iban = "0182041743" + ccc;

      var newAccount = {
        "iban": iban,
        "acid": nextId,
        "uid": req.body.uid,
        "balance": 0
      };

      httpClient.post("accounts?" + mLabAPIKey, newAccount,
        function (err, resMlab, body) {
          console.log("Cuenta creada correctamente");
          res.status(201).send({ "msg": "Cuenta creada correctamente", "acid": nextId });
        }
      );
    }
  );
}

/**
 * Description
 * @method updateAccount
 * @param {} req
 * @param {} res
 * @return actualiza el saldo de una cuenta
 */
function updateAccount(req, res) {
  console.log('PUT /apitechuarr9ed/accounts');
  console.log("updateAccount: " + req.body);

  var response = {};
  response.msg = "";

  var dataMove = {
    "acid": req.body.acid,
    "tipo": req.body.tipo,
    "importe": req.body.importe
  }

  console.log(dataMove.acid);
  acac
  query = 'q={"acid": ' + dataMove.acid + '}';

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("cliente creado");

  httpClient.get("accounts?"
    + query
    + "&"
    + mLabAPIKey,
    function (err, resMlab, body) {
      //si llega un error del servidor remoto ->
      if (err) {
        response = { "msg": "Error obteniendo usuario" };
        res.status(500).send({ "msg": "Error obteniendo usuario" });
      } else {
        if (body.length > 0) {
          response = body[0];
        } else {
          var response = { "msg": "Usuario no encontrado" };
          res.status(404).send({ "msg": "Usuario no encontrado" });
        }
      }

      var newBalance = response.balance;

      if (dataMove.tipo == "ABONO") {
        newBalance += dataMove.importe;
      } else if (dataMove.tipo == "CARGO") {
        newBalance -= dataMove.importe;
      }

      var putBody = '{"$set":{"balance":' + newBalance + '}}';

      httpClient.put("accounts?"
        + query
        + "&"
        + mLabAPIKey,
        JSON.parse(putBody),
        function (errPUT, resMlabPUT, bodyPUT) {
          if (errPUT) {
            response = { "msg": "Error actualizando balance de la cuenta" };
            res.status(500).send({ "msg": "Error actualizando balance de la cuenta" });
          } else {
            if (bodyPUT.length > 0) {
              response = bodyPUT[0];
              res.status(201).send(response);
            } else {
              response = { "msg": "Cuenta no encontrada" };
              res.status(404).send({ "msg": "Cuenta no encontrada" });
            }
          }
        }
      );
      console.log(response);

    }
  );
}

module.exports.getAccounts = getAccounts;
module.exports.getMaxAccount = getMaxAccount; //For test max id account
module.exports.getAccountsByUid = getAccountsByUid;
module.exports.createAccount = createAccount;
module.exports.updateAccount = updateAccount;
