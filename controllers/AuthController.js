const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechuarr9ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

/**
 * Description
 * @method login
 * @param {} req
 * @param {} res
 * @return
 */
function login(req, res) {
  console.log('PUT /apitechuarr9ed/login');

  var response = {};
  response.msg = "";

  var userLogado = {
    "nif": req.body.nif,
    "password": req.body.password
  }

  query = 'q={"nif": "' + userLogado.nif + '"}';
  //console.log(query);

  var httpClient = requestJson.createClient(baseMlabURL);
  //console.log("cliente creado");

  httpClient.get("users?"
    + query
    + "&"
    + mLabAPIKey,
    function (err, resMlab, body) {
      //si llega un error del servidor remoto ->
      if (err) {
        response = { "msg": "Error obteniendo usuario" };
        res.status(500).send({ "msg": "Error obteniendo usuario" });
      } else {
        if (body.length > 0) {
          response = body[0];
          //console.log("Pass body: " + userLogado.password + " Pass Mlab: " + response.password);

          var loginOK = crypt.checkPass(userLogado.password, response.password);

          response.msg = loginOK ?
            "Login correcto" : "Login incorrecto";

          console.log("Login msg: " + response.msg);
          if (loginOK) {
            var putBody = '{"$set":{"logged":true}}';

            httpClient.put("users?"
              + query
              + "&"
              + mLabAPIKey,
              JSON.parse(putBody),
              function (errPUT, resMlabPUT, bodyPUT) {
                if (errPUT) {
                  response = { "msg": "Error actualizando logged de usuario" };
                  res.status(500).send({ "msg": "Error actualizando logged de usuario" });
                } else {
                  if (body.length > 0) {
                    response = body[0];
                    res.status(201).send(response);
                  } else {
                    var response = { "msg": "Usuario no encontrado" };
                    res.status(404).send({ "msg": "Usuario no encontrado" });
                  }
                }
              }
            );
          }
        } else {
          response = { "msg": "Usuario no encontrado" };
          res.status(404).send({ "msg": "Usuario no encontrado" });
        }
      }      
    }
  );
}

/**
 * Description
 * @method logout
 * @param {} req
 * @param {} res
 * @return
 */
function logout(req, res) {
  console.log("PUT /apitechuarr9ed/logout");
  console.log("uid es: " + req.body.uid);

  var response = {};
  response.msg = "";

  var uid = req.body.uid;
  var query = 'q={"uid": ' + uid + '}';
  console.log("Query: " + query);

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("cliente creado");

  httpClient.get("users?"
    + query
    + "&"
    + mLabAPIKey,
    function (err, resMlab, body) {
      if (err) {
        response = { "msg": "Error obteniendo usuario" };
        res.status(500);
      } else {
        if (body.length > 0) {
          var element = body[0];
          console.log("user id:" + element.uid + " req.uid: " + uid + " logged: " + element.logged);

          var logoutOK = element.uid == uid && element.logged;

          response.msg = logoutOK ?
            "Logout correcto" : "Logout incorrecto";

          if (logoutOK) {
            var putBody = '{"$unset":{"logged":false}}';
            response.uid = element.uid;
            httpClient.put("users?"
              + query
              + "&"
              + mLabAPIKey,
              JSON.parse(putBody),
              function (errPUT, resMlabPUT, bodyPUT) {
                if (errPUT) {
                  response = { "msg": "Error actualizando logged de usuario" };
                  res.status(500);
                } else {
                  if (body.length <= 0) {
                    response = { "msg": "Usuario no encontrado" };
                    res.status(404);
                  }
                }
              }
            );
          }
        } else {
          response = { "msg": "Usuario no encontrado" };
          res.status(404);
        }
      }
      console.log("salida de logout: " + response);
      res.send(response);
    }
  );
}

module.exports.login = login;
module.exports.logout = logout;
