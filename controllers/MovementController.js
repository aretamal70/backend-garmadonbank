const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');

const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechuarr9ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

/**
 * Description
 * @method getMovementsByAccountId
 * @param {} req
 * @param {} res
 * @return devuelve los movimientos de la cuenta seleccionada
 */
function getMovementsByAccountId(req, res) {
  console.log('GET /apitechuarr9ed/movements/:acid');
  console.log("acid seleccionado " + req.params.acid);
  var acid = req.params.acid;

  var response = {};
  response.msg = "";

  var query = 'q={"acid":' + acid + '}';
  console.log(query);

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("cliente MLAB creado");

  httpClient.get("movements?" + query + "&" + mLabAPIKey,
    function (err, resMlab, body) {
      if (err) {
        response = { "msg": "Error obteniendo movimientos de la cuenta" };
        res.status(500);
      } else {
        response = body;
        res.status(200);
      }

      console.log(response);
      res.send(response);
    }
  );
}

/**
 * Description
 * @method getMovements
 * @param {} req
 * @param {} res
 * @return devuelve todas los movimientos
 */
function getMovements(req, res) {
  console.log('GET /apitechuarr9ed/movements');

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("cliente MLAB creado");

  httpClient.get("movements?" + mLabAPIKey,
    function (err, resMlab, body) {
      var response = !err ? body : {
        "msg": "Error obteniendo todos los movimientos"
      }

      res.send(response);
    }
  );
}

/**
 * Description
 * @method getMaxIdMovement
 * @param {} req
 * @param {} res
 * @return obtiene el id maximo de movimientos
 */
function getMaxIdMovement(req, res) {
  console.log('GET /apitechuarr9ed/movements2');

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("cliente creado");

  httpClient.get("movements?s={id: -1}&l=1&" + mLabAPIKey,
    function (err, resMlab, body) {
      var response = !err ? body : {
        "msg": "Error obteniendo movimientos"
      }

      var nextId = 1; //Si no hay se empieza por id 1

      if (body[0].id != undefined) {
        nextId = body[0].id + 1;
      }
      console.log("nextId mvtos: " + nextId);
      res.send(response);
    }
  );
}

/**
 * Description
 * @method createMovement
 * @param {} req
 * @param {} res
 * @return crea un movimiento asociado a la cuenta del cliente
 */
function createMovement(req, res) {
  console.log('POST /apitechuarr9ed/movements');
  console.log("acid: " + req.body.acid);
  console.log("tipo: " + req.body.tipo);
  console.log("importe: " + req.body.importe);
  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("cliente creado");

  //obtiene new id de movimiento
  httpClient.get("movements?s={mid: -1}&l=1&" + mLabAPIKey,
    function (err, resMlab, body) {
      var response = !err ? body :
        {
          "msg": "Error obteniendo movimientos"
        }

      var nextId = 1; //Si no hay se empieza por id 1
      console.log(body[0]);
      //incrementas si ya hay id....
      if (body[0] != undefined) {
        nextId = body[0].mid + 1;
      }
      console.log("nextId mvtos: " + nextId);

      var newMove = {
        "mid": nextId,
        "acid": req.body.acid,
        "tipo": req.body.tipo,
        "importe": req.body.importe
      };

      //inserta nuevo movimiento
      httpClient.post("movements?" + mLabAPIKey, newMove,
        function (errPOST, resMlab, bodyPOST) {
          console.log("Movimiento creado");

          //Consulta de cuentas para actualizar balance
          var query = 'q={"acid":' + newMove.acid + '}';
          console.log("query: " + query);
          httpClient.get("accounts?"
            + query
            + "&"
            + mLabAPIKey,
            function (errGETCCC, resMlab, bodyGETCCC) {
              //si llega un error del servidor remoto en el POST->
              if (errGETCCC) {
                response = { "msg": "Error obteniendo la cuenta asociada" };
                res.status(500).send({ "msg": "Error obteniendo la cuenta asociada" });
              } else {
                if (bodyGETCCC.length > 0) {
                  response = bodyGETCCC[0];
                } else {
                  response = { "msg": "Cuenta asociada no encontrada" };
                  res.status(404).send({ "msg": "Cuenta asociada no encontrada" });;
                }
              }

              var newBalance = parseFloat(response.balance);
              console.log("newBalance: " + newBalance);
              if (newMove.tipo == "ABONO") {
                newBalance += parseFloat(newMove.importe);
              } else if (newMove.tipo == "CARGO") {
                newBalance -= parseFloat(newMove.importe);
              }

              var putBody = '{"$set":{"balance":' + newBalance + '}}';
              console.log("putBody: " + putBody);
              httpClient.put("accounts?"
                + query
                + "&"
                + mLabAPIKey,
                JSON.parse(putBody),
                function (errPUT, resMlabPUT, bodyPUT) {
                  if (errPUT) {
                    console.log("Error actualizando balance de la cuenta");
                    response = { "msg": "Error actualizando balance de la cuenta" };
                    res.status(500).send({ "msg": "Error actualizando balance de la cuenta" });
                  } else {
                    response = bodyPUT[0];
                    console.log("Movimiento añadido");
                    res.status(201).send({ "msg": "Movimiento generado", "newBalance": newBalance });
                  }
                }
              );

            }
          );
        }
      );


    }
  );
}

module.exports.getMovementsByAccountId = getMovementsByAccountId;
module.exports.getMovements = getMovements;
module.exports.createMovement = createMovement;
module.exports.getMaxIdMovement = getMaxIdMovement; //Para testeo de id max
