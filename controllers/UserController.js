const crypt = require('../crypt');
const requestJson = require('request-json');

const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechuarr9ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

/**
 * Description
 * @method getUsersById
 * @param {} req
 * @param {} res
 * @return devuelve un usuario en base a su identificador
 */
function getUsersById(req, res) {
  console.log('GET /apitechuarr9ed/users/:uid');
  //console.log("id introducido " + req.params.id);
  var uid = req.params.uid;

  var query = 'q={"uid":' + uid + '}';
  console.log(query);

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("cliente creado");

  httpClient.get("users?" + query + "&" + mLabAPIKey,
    function (err, resMlab, body) {
      if (err) {
        var response = { "msg": "Error obteniendo usuario" };
        res.status(500);
      } else {
        if (body.length > 0) {
          var response = body[0];
          res.status(200)
        } else {
          var response = { "msg": "Usuario no encontrado" };
          res.status(404);
        }
      }

      //console.log(response);
      res.send(response);
    }
  );
}

/**
 * Description
 * @method getUsers
 * @param {} req
 * @param {} res
 * @return devuelve la lista de usuarios
 */
function getUsers(req, res) {
  console.log('GET /apitechuarr9ed/users');

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("cliente creado");

  httpClient.get("users?" + mLabAPIKey,
    function (err, resMlab, body) {
      var response = !err ? body : {
        "msg": "Error obteniendo usuarios"
      }

      res.send(response);
    }
  );
}

/**
 * Description
 * @method createUser
 * @param {} req
 * @param {} res
 * @return crea un nuevo usuario
 */
function createUser(req, res) {
  console.log('POST /apitechuarr9ed/users');
  //Revisar campos obligatorios
  console.log(req.body.nif);
  if (req.body.nif == undefined) {
    res.status(401).send({ "msg": "Todos los campos son obligatorios" });
  } else {
    var httpClient = requestJson.createClient(baseMlabURL);
    console.log("cliente creado");
    httpClient.get("users?s={uid: -1}&l=1&" + mLabAPIKey,
      function (err, resMlab, body) {
        var response = !err ? body : {
          "msg": "Error obteniendo usuarios"
        }

        var nextUid = 1; //Si no hay se empieza por id 1
        if (body[0] != undefined) {
          nextUid = body[0].uid + 1;
        }
        console.log("nextId user: " + nextUid)

        var newUser = {
          "uid": nextUid,
          "nif": req.body.nif,
          "first_name": req.body.first_name,
          "last_name": req.body.last_name,
          "email": req.body.email,
          "tlfn": req.body.tlfn,
          "password": crypt.hash(req.body.password)
        };

        httpClient.post("users?" + mLabAPIKey, newUser,
          function (errUsu, resMlab, bodyUsu) {
            console.log("Usuario creado correctamente");
            res.status(201).send({ "msg": "Usuario creado correctamente", "uid": nextUid });
          }
        );

        console.log("usuario añadido");
      }
    );
  }
}

module.exports.getUsers = getUsers;
module.exports.getUsersById = getUsersById;
module.exports.createUser = createUser;
