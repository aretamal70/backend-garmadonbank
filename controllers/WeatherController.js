const requestJson = require('request-json');
const openWeatherMapAPIKey = "APPID=" + process.env.OPENWEATHERMAP_API_KEY;
const baseOpenWeatherURL = "http://api.openweathermap.org/data/2.5/"
function getWeather(req, res) {
    console.log("getWeather");
    var httpClient = requestJson.createClient(baseOpenWeatherURL);
    httpClient.get("find?q=Madrid,es&units=metric" + "&" + openWeatherMapAPIKey,
        function (error, response, body) {
            if (!error && response.statusCode == 200) {
                response = body;
                //console.log(response);
                res.send(response);
            }
        });
}

module.exports.getWeather = getWeather;