const bcrypt = require('bcrypt');

/**
 * Description
 * @method hash
 * @param {} data
 * @return CallExpression
 */
function hash(data){
    console.log("Hashing data");
    return bcrypt.hashSync(data, 10);
}

/**
 * Description
 * @method checkPass
 * @param {} passPlainText
 * @param {} passDBHashed
 * @return CallExpression
 */
function checkPass(passPlainText, passDBHashed){
  console.log("Comprobando password");
  return bcrypt.compareSync(passPlainText, passDBHashed);
}

module.exports.hash = hash;
module.exports.checkPass = checkPass;
