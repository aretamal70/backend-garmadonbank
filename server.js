require('dotenv').config();
const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');
const accountController = require('./controllers/AccountController');
const movementController = require('./controllers/MovementController');
const weatherController = require('./controllers/WeatherController');

//Para habilitar la conexion entre dominios
var enableCORS = function (req, res, next) {
	res.set("Access-Control-Allow-Origin", "*");
	res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
	res.set("Access-Control-Allow-Headers", "Content-Type");
	next();
}

app.use(express.json());
app.use(enableCORS);

app.listen(port);
console.log("API escuchando en el puerto " + port);

app.get('/apitechuarr9ed/weather', weatherController.getWeather);

app.put('/apitechuarr9ed/login', authController.login);
app.put('/apitechuarr9ed/logout', authController.logout);

app.get('/apitechuarr9ed/users', userController.getUsers);
app.get('/apitechuarr9ed/users/:uid', userController.getUsersById);
app.post('/apitechuarr9ed/users', userController.createUser);

app.get('/apitechuarr9ed/accounts', accountController.getAccounts);
app.get('/apitechuarr9ed/accounts/:uid', accountController.getAccountsByUid);
app.post('/apitechuarr9ed/accounts', accountController.createAccount);
app.put('/apitechuarr9ed/accounts', accountController.updateAccount); //For test purpose
app.get('/apitechuarr9ed/accounts2', accountController.getMaxAccount); //For test max id account

app.get('/apitechuarr9ed/movements/:acid', movementController.getMovementsByAccountId);
app.get('/apitechuarr9ed/movements', movementController.getMovements);
app.post('/apitechuarr9ed/movements', movementController.createMovement);
app.get('/apitechuarr9ed/movements2', movementController.getMaxIdMovement); // For test max id movement
